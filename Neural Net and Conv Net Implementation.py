# -*- coding: utf-8 -*-

"""
Student number: 201271021
"""

from tensorflow.keras.models import Sequential 
from tensorflow.keras.layers import Dense, Activation, Flatten, Conv2D, MaxPooling2D
from sklearn import datasets
from sklearn.model_selection import train_test_split
import numpy as np
import math
import matplotlib.pyplot as plt
from tensorflow.keras.models import load_model
from sklearn.neighbors import KNeighborsClassifier

#My assignment 1 implementation of KNN, here so it can be used to complete part two of assignment 2.
class KNN:#A class for my own implementation of KNN
    def __init__(self,X,y,X2,y2):
        self.X_train = X
        self.y_train = y
        self.X_test = X2
        self.y_test = y2
        
    def predictAll(self):
        correctGuesses = 0
        euclideanArray = [] #Array to hold euclidean distances for each number tested
        neighbours = []#Array to hold k nearest neighbours of each test instance.
        guesses = []#Array to hold predictions of each test instance.
        
        for i in range(len(self.X_test)):
            euclideanArray.append(self.euclideanDist(self.X_train,self.X_test[i]))
        
        for i in range(len(euclideanArray)):
            neighbours.append(self.getNeighbours(1,euclideanArray[i]))
            
        for i in range(len(neighbours)):
            guesses.append(self.getResponse(neighbours[i],self.y_train))
            
        for i in range(len(guesses)):
            if(guesses[i] == self.y_test[i]):
                correctGuesses = correctGuesses + 1
                
        return self.accuracy(correctGuesses,guesses)
    
    def predictAllArray(self):
        correctGuesses = 0
        euclideanArray = [] #Array to hold euclidean distances for each number tested
        neighbours = []#Array to hold k nearest neighbours of each test instance.
        guesses = []#Array to hold predictions of each test instance.
        
        for i in range(len(self.X_test)):
            euclideanArray.append(self.euclideanDist(self.X_train,self.X_test[i]))
        
        for i in range(len(euclideanArray)):
            neighbours.append(self.getNeighbours(1,euclideanArray[i]))
            
        for i in range(len(neighbours)):
            guesses.append(self.getResponse(neighbours[i],self.y_train))
        
        return guesses
        
        
    def euclideanDist(self,X_train,X_test):#Parameters: X_train 2D array of 1794 entries, X_test 1D array of an instance
        distances = []
        sumOfDistances = 0
        for x in range(len(X_train)): 
            for i in range(64):
                sumOfDistances = sumOfDistances + (((X_train[x][i]-X_test[i])**2)**0.5)
            
            distances.append(sumOfDistances)
            sumOfDistances = 0
        return distances
    
    def getNeighbours(self,k, distances):
        neighbours = []
        for i in range(k):#Make K passes to find k nearest neighbours
            smallest = distances[0]
            smallestIndex = 0
            for j in range(len(distances)):#Loop through distances array and find smallest element
                if(smallest>distances[j]):
                    smallestIndex = j
                    smallest = distances[j]
                
            neighbours.append(smallestIndex)#Save index for predictions
            distances[smallestIndex] = 10000 #Make entry arbitrarily large so that next loop doesn't pick it up again.
            
        return neighbours
    
    def getResponse(self,neighbours,y_train):
        predictions = []
        predictionCount = [0,0,0,0,0,0,0,0,0,0]
        for i in range(len(neighbours)):
            predictions.append(self.y_train[(neighbours[i])])
        
            
        for i in range(len(predictions)):
            predictionCount[(predictions[i])] = predictionCount[(predictions[i])]+1
            
        projected = predictionCount[0]
        prediction = 0;
        for i in range(len(predictionCount)):
            if(projected<predictionCount[i]):
                projected = predictionCount[i]
                prediction = i
                
        return prediction
    
    def accuracy(self,correctGuesses,numOfGuesses):#Takes the array containing all correct guesses so that we can find difference between that number and num of entries to calc accuracy.
        accuracy = (correctGuesses/(len(numOfGuesses)))*100
        return accuracy
    
    def predict(self, index):#Fullfills the requirements of F5
        distances = self.euclideanDist(self.X_train,self.X_test[index])
        neighbours = self.getNeighbours(1,distances)
        prediction = self.getResponse(neighbours,self.y_train)
        print("We predict that entry",index,"in the model is a number:",prediction)






#Assignment 2: Start Point
data = datasets.load_digits()
X_train, X_test, y_train, y_test = train_test_split(data.data, data.target, test_size=0.20, random_state=0)
X_train1 = X_train #Storing the default split of X_train as X_train will have to be reshaped for CNN
X_test2 = X_test #Storing the default split of X_test as X_test will have to be reshaped for CNN

X_train = X_train.reshape(1437,8,8,1)#Reshape for the convolutional layer.
X_test = X_test.reshape(360,8,8,1) #Reshape to four dimensions for convolutional layer.

def arrayAccuracy(arr1, arr2):
        count = 0
        for i in range(len(arr1)):
            if(arr1[i] == arr2[i]):
                count = count + 1
                
        dataSize = len(arr1)
        accuracy = (count/dataSize)*100
        return accuracy
   
    
def convNeuralNetworkTraining():
    #Convolutional Neural Network
    model = Sequential() #Neural network propagates forwards as it is 'sequential'
    model.add(Conv2D(128,(3,3)))#Add first convolutional layer consisting of 64 nodes and a window size of 3x3
    model.add(Activation('relu')) #Activation function for this layer
    model.add(MaxPooling2D(pool_size = (2,2)))
    
    model.add(Conv2D(64,(1,1)))#Add first convolutional layer consisting of 64 nodes and a window size of 3x3
    model.add(Activation('relu')) #Activation function for this layer
    model.add(MaxPooling2D(pool_size = (2,2)))
    
    
    model.add(Flatten())
    model.add(Dense(32))
    model.add(Activation('relu'))
    
    model.add(Dense(10))
    model.add(Activation('sigmoid'))#Use of signmoid function, giving a value between 0 and 1 better than a simple stepper function.
    
    model.compile(loss = 'sparse_categorical_crossentropy', optimizer = 'adam', metrics = ['accuracy'])
    model.fit(X_train, y_train,epochs = 10, batch_size = 32)
    model.save('conv_neural_network.h5')

    
def neuralNetworkTraining():
    #Standard Neural Network
    model2 = Sequential()
    model2.add(Flatten(input_shape=(64, )))
    model2.add(Dense(128, activation = 'relu'))
    model2.add(Dense(128, activation = 'relu'))
    model2.add(Dense(64, activation = 'relu'))
    model2.add(Dense(10,activation = 'sigmoid'))
    model2.compile(loss = 'sparse_categorical_crossentropy', optimizer = 'adam', metrics = ['accuracy'])
    model2.fit(X_train1,y_train, epochs = 10, batch_size = 32)
    model2.save('neural_network.h5')


def testNeuralNetwork():
    nn = load_model('neural_network.h5')
    predictions = nn.predict(([X_test2]))
    predictions = [np.argmax(i) for i in predictions] #Takes the argmax of the maximum value in the array revealing the actual predicted classes.
    accuracy = arrayAccuracy(predictions, y_test)
    print('The accuracy of the neural network is: '+str(accuracy)+'%')
    
    return predictions


def testConvNeuralNetwork():
    cnn = load_model('conv_neural_network.h5')
    predictions = cnn.predict((X_test))
    predictions = [np.argmax(i) for i in predictions] #Takes the argmax of the maximum value in the array revealing the actual predicted classes.
    accuracy = arrayAccuracy(predictions, y_test)
    print("The accuracy of the convolutional neural network is: "+str(accuracy)+"%")
    
    return predictions
    

def modelRunner(models, trainSamples,trainLabels,foldData,foldLabels,cnnCheck):#Used to run each model in cross validation.
    if(cnnCheck == True):#CNN requires reshaping hence this parameter 'cnnCheck.'
        trainSamples = np.asarray(trainSamples)
        trainLabels = np.asarray(trainLabels)
        foldData = np.asarray(foldData)
        foldLabels = np.asarray(foldLabels)
        foldData = foldData.reshape(179,8,8,1)
        trainSamples = trainSamples.reshape(716,8,8,1)
        models.fit(trainSamples,trainLabels,epochs = 5,batch_size = 32)
        predictions = models.predict([foldData])
        predictions = [np.argmax(i) for i in predictions]
        accuracy = arrayAccuracy(predictions, foldLabels)
        return accuracy
    
    else:
        
        trainSamples = np.asarray(trainSamples)
        trainLabels = np.asarray(trainLabels)
        foldData = np.asarray(foldData)
        foldLabels = np.asarray(foldLabels)
        models.fit(trainSamples,trainLabels,epochs = 5,batch_size = 32 )
        predictions = models.predict([foldData])
        predictions = [np.argmax(i) for i in predictions]
        accuracy = arrayAccuracy(predictions, foldLabels)
        return accuracy
   

def crossValidation():#Here we will implement 5 fold cross validation
    #Need to split data.data and data.target into 5 equal blocks
    totalData = data.data
    totalTarget = data.target
    dataBlocks = [] #Array to hold 5 blocks of data
    targetBlocks = []#Array to hold 5 blocks of labels
    blockSize = math.floor((len(totalData)/10))
    indexHandler = 0
    tmpData = []
    tmpTarget = []
    testIndex = 0
    trainData = []
    testData = []
    testLabels = []
    trainLabels = []
    knnScore = []
    cnnScore = []
    nnScore = []
    cnnModel = load_model('conv_neural_network.h5')
    nnModel = load_model('neural_network.h5')
    libraryKnn = KNeighborsClassifier(n_neighbors = 1)#Library model initalised here
    libraryKnnScore = []
        
    for i in range(5):    
        for j in range(blockSize):
            tmpData.append(totalData[j+indexHandler])
            tmpTarget.append(totalTarget[j+indexHandler])
        
        dataBlocks.append(tmpData)
        targetBlocks.append(tmpTarget)
        indexHandler = indexHandler+blockSize-1
        tmpData = []
        tmpTarget = []
        
    for k in range(5):
        for i in range(5):
            for j in range(blockSize):
                if(i == testIndex):
                    testData.append(dataBlocks[i][j])
                    testLabels.append(targetBlocks[i][j])
    
                else:
                    trainData.append(dataBlocks[i][j])
                    trainLabels.append(targetBlocks[i][j])
            
        
        nnScore.append(modelRunner(nnModel,trainData,trainLabels,testData,testLabels,False))
        cnnScore.append(modelRunner(cnnModel,trainData,trainLabels,testData,testLabels,True))
        myKNN = KNN(trainData,trainLabels,testData,testLabels)
        knnScore.append(myKNN.predictAll())
        libraryKnn.fit(trainData,trainLabels)
        predictions = libraryKnn.predict(testData)
        libraryKnnScore.append(arrayAccuracy(predictions,testLabels))
        
        trainData = []
        trainLabels = []
        testData = []
        testLabels = []
        
    averageLibraryKNN = (sum(libraryKnnScore)/len(libraryKnnScore))
    averageNN = (sum(nnScore)/len(nnScore))
    averageKNN = (sum(knnScore)/len(knnScore))
    averageCNN = (sum(cnnScore)/len(cnnScore))
    if(averageNN > max(averageCNN, averageKNN,averageLibraryKNN)):
        print("The neural network is the best method according to 5 fold validation with a score of: "+str(averageNN)+"%\n"+"The resulting average for the convolutional neural network was: "+str(averageCNN)+"%\n"+"The resulting average for the KNN was: "+str(averageKNN)+"%n"+"The resulting average for the library KNN was: "+str(averageLibraryKNN)+"%")
    elif(averageCNN > max(averageNN, averageKNN, averageLibraryKNN)):
        print("The convolutional neural network is the best method according to 5 fold validation with a score of: "+str(averageCNN)+"%\n"+"The resulting average for the neural network was: "+str(averageNN)+"%\n"+"The resulting average for the KNN was: "+str(averageKNN)+"%\n"+"The resulting average for the library KNN was: "+str(averageLibraryKNN)+"%")
    elif(averageKNN > max(averageNN,averageLibraryKNN,averageCNN)):
        print("My KNN is the best method according to 5 fold validation with a score of: "+str(averageKNN)+"%\n"+"The resulting average for the neural network was: "+str(averageNN)+"%\n"+"The resulting average for the CNN was: "+str(averageCNN)+"%\n"+"The resulting average for the library KNN was: "+str(averageLibraryKNN)+"%")
    else:
        print("The Library K-Nearest neighbour algorithm is the method according to 5 fold validation with a score of: "+str(averageLibraryKNN)+"%\n"+"The resulting average for the convolutional neural network was: "+str(averageCNN)+"%\n"+"The resulting average for the neural network was: "+str(averageNN)+"%\n"+"The resulting average for the library KNN was: "+str(averageKNN)+"%")
        
            
def confusionMatrix(predictions, evidence):
    #Confusion matrix 10 by 10 as 10 possible classifactions first index corresponds to predictions,
    #second corresponds to true values. Hence the vertical corresponds to the predictions and the 
    #horizontal corresponds to the true values.
    confusionMatrix = [[0,0,0,0,0,0,0,0,0,0],
                       [0,0,0,0,0,0,0,0,0,0],
                       [0,0,0,0,0,0,0,0,0,0],
                       [0,0,0,0,0,0,0,0,0,0],
                       [0,0,0,0,0,0,0,0,0,0],
                       [0,0,0,0,0,0,0,0,0,0],
                       [0,0,0,0,0,0,0,0,0,0],
                       [0,0,0,0,0,0,0,0,0,0],
                       [0,0,0,0,0,0,0,0,0,0],
                       [0,0,0,0,0,0,0,0,0,0]]
    
    for i in range(len(predictions)):
        confusionMatrix[predictions[i]][evidence[i]] = confusionMatrix[predictions[i]][evidence[i]] + 1
    
    for i in range(10):
        for j in range(10):
            print(str(confusionMatrix[i][j]) + " ", end = "")
        print()
        
    return confusionMatrix
   

    
  
crossValidation()
myKNN = KNN(X_train1,y_train,X_test2,y_test)
libraryKnn = KNeighborsClassifier(n_neighbors = 1)
libraryKnn.fit(X_train1,y_train)
predictions = libraryKnn.predict(X_test2)
print("Confusion Matrix for the Neural Network is:")
confusionMatrix(testNeuralNetwork(),y_test)
print("Confusion Matrix for the Convolutional Neural Network is:")
confusionMatrix(testConvNeuralNetwork(),y_test)
print("Confusion Matrix for my KNN implementation is:")
confusionMatrix(myKNN.predictAllArray(),y_test)
print("Confusion Matrix for the library KNN implementation is:")
confusionMatrix(predictions,y_test)


            
    
    

